module.exports = {
  css: {
    loaderOptions: {
      sass: {
        data: `
          @import "@/scss/_variables.scss";
          @import "@/scss/_mixins.scss";
          @import "@/scss/_sidebar.scss";
          @import "@/scss/_feature.scss";
          @import "@/scss/_footer.scss";
          @import "@/scss/_gallery.scss";
          @import "@/scss/_header.scss";
          @import "@/scss/_homes.scss";
          @import "@/scss/_realtors.scss";
          @import "@/scss/_story.scss";
          @import "@/scss/_typography.scss";
        `
      }
    }
  }
};
